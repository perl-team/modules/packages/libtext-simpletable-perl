libtext-simpletable-perl (2.07-2) UNRELEASED; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2020 16:46:31 +0100

libtext-simpletable-perl (2.07-1) unstable; urgency=medium

  * Import upstream version 2.05.
  * Import upstream version 2.07.
  * Update (build) dependencies.
  * Declare compliance with Debian Policy 4.1.5.
  * Fix hashbang in example script. Thanks to lintian.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Jul 2018 19:52:46 +0200

libtext-simpletable-perl (2.04-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Nathan Handler from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Add debian/upstream/metadata.
  * Import upstream version 2.04.
  * Update upstream contact.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 May 2018 18:13:46 +0200

libtext-simpletable-perl (2.03-1) unstable; urgency=medium

  * Team upload.

  [ Jonathan Yu ]
  * New upstream release
  * Standards-Version 3.8.4 (drop perl version dep)
  * Add myself to Uploaders and Copyright
  * Update to new DEP5 copyright format

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Nathan Handler ]
  * Email change: Nathan Handler -> nhandler@debian.org

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * Update upstream copyright stanza for switch to Artistic-2.0.
    Update License field and add explicitly a copy of the Artistic 2.0
    license text to debian/copyright file.
  * Convert package to '3.0 (quilt)' source package format
  * Convert debian/rules to a tiny rules file
  * Bump Debhelper compat level to 8.
    Adjust versioned Build-Depends on debhelper to (>= 8)
  * Drop empty line from debian/watch file
  * Declare compliance with Debian policy 3.9.6
  * Drop libtest-pod-perl and libtest-pod-coverage-perl from
    Build-Depends-Indep.
    Drop libtest-pod-perl and libtest-pod-coverage-perl so that POD and
    POD coverage author tests are not run.
  * Install examples into binary package
  * Explicitly refer to GPL-1 license text in common-licenses
  * Declare package as autopkgtestable

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 13 Aug 2015 13:51:41 +0200

libtext-simpletable-perl (1.2-1) unstable; urgency=low

  * New upstream release

 -- Nathan Handler <nhandler@ubuntu.com>  Sat, 04 Jul 2009 03:07:22 +0000

libtext-simpletable-perl (1.1-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Nathan Handler ]
  * New upstream release
  * debian/watch:
    - Update to ignore development releases.
  * debian/control:
    - Add myself to list of Uploaders
    - Bump Standards-Version to 3.8.2
  * debian/rules:
    - Refresh to use new format
  * debian/copyright:
    - Refresh to use new format
    - Add myself as copyright holder for debian/* files

  [ gregor herrmann ]
  * Activate additional tests (debian/{rules,control}).
  * debian/control: improve short/long description.

 -- Nathan Handler <nhandler@ubuntu.com>  Fri, 03 Jul 2009 12:57:38 +0000

libtext-simpletable-perl (0.05-1) unstable; urgency=low

  * New upstream release
  * Move package to Debian Perl Group, set proper headers.
  * Convert package to debhelper 7, use new debian/rules file
  * Update debian/watch file to new schema

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Tue, 14 Oct 2008 15:31:35 +0200

libtext-simpletable-perl (0.03-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 17 Jan 2006 23:14:06 +0100

libtext-simpletable-perl (0.02-2) unstable; urgency=low

  * debian/control - description changed to keep Debian Policy p. 5.6.13

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 15 Nov 2005 21:40:36 +0100

libtext-simpletable-perl (0.02-1) unstable; urgency=low

  * Initial Release (closes: #338480)

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 10 Nov 2005 12:37:01 +0100
